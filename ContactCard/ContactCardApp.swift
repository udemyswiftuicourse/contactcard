//
//  ContactCardApp.swift
//  ContactCard
//
//  Created by Sergio Andres Rodriguez Castillo on 09/01/24.
//

import SwiftUI

@main
struct ContactCardApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
