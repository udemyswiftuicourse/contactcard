//
//  ContentView.swift
//  ContactCard
//
//  Created by Sergio Andres Rodriguez Castillo on 09/01/24.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        ZStack{
            Color(red: 0.09, green: 0.63, blue: 0.52)
                .edgesIgnoringSafeArea(.all)
            VStack {
                Image("profile")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 150.0, height: 150.0)
                    .clipShape(Circle())
                    .overlay(
                        Circle().stroke(Color.white, lineWidth: 5)
                    )
                Text("Sergio Rodriguez")
                    .font(Font.custom("Pacifico-Regular", size: 40))
                    .bold()
                .foregroundColor(.white)
                Text("iOS Developer")
                    .foregroundColor(.white)
                    .font(.system(size: 25))
                Divider()
                InfoView(text: "+52 123 456 7890", imageName: "phone.fill")
                InfoView(text: "email@email.com", imageName: "envelope.fill")
            }
        }
    }
}

#Preview {
    ContentView()
}
