//
//  InfoView.swift
//  ContactCard
//
//  Created by Sergio Andres Rodriguez Castillo on 09/01/24.
//

import SwiftUI

struct InfoView: View {
    let text: String
    let imageName: String
    
    var body: some View {
        RoundedRectangle(cornerRadius: 25)
            .fill(Color.white)
            .frame(height: 50.0)
            .overlay(HStack {
                Image(systemName: imageName)
                    .foregroundColor(.green)
                Text(text)
                    .foregroundColor(Color("Info Color"))
            })
            .padding(.all)
    }
}

#Preview {
    InfoView(text: "Text", imageName: "phone.fill")
        .previewLayout(.sizeThatFits)
}
